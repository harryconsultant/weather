# WEATHER APP #

![IMG_9594.PNG](https://bitbucket.org/repo/gkynM55/images/3059481138-IMG_9594.PNG)


This app displays current location's 5 day weather forecast.
It uses OpenWeatherMap Api to retrieve weather information.

The App displays following:

* Current location
* Current location's maximum temperature
* Current location's minimum temperature
* Current location's Sun rise and Sun set timings
* Last updated time
* Refresh button
* Weather forecast in that week

On launch of Weather App. Please tap on refresh button to get accurate location's weather forecast.
Slide below to see weather forecast of the week.