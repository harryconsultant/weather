//
//  WEDelegate.h
//  Weather
//
//  Created by Hari
//  Copyright © 2017. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WEDelegate : UIResponder<UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
