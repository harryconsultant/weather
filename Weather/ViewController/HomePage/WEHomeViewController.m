//
//  WEHomeViewController.m
//  Weather
//
//  Created by Hari
//  Copyright © 2017. All rights reserved.
//

#import "WEHomeViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "WEWeekCollectionViewCell.h"

@interface WEHomeViewController ()<CLLocationManagerDelegate, UICollectionViewDelegate, UICollectionViewDelegate>{
    NSMutableArray *weatherArray;
    NSDate *lastUpdated;
    CLLocationManager *locationManager;
}

@end

@implementation WEHomeViewController

#pragma - mark View Appear

//Load View
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Registor Xib to cell
       [_ibWeekWeather registerNib:[UINib nibWithNibName:@"WEWeekCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CustomCell"];
    
    //Create timer to last updated time
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector: @selector(updateLastUpdatedTime)
                                                    userInfo: nil
                                                     repeats: YES];
    NSLog(@"%@",timer);
}

//View Will Appear
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

//View Did Appear
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self didTapOnRefresh:nil];
}

#pragma - mark Button Action
-(IBAction)didTapOnRefresh:(id)sender{
    
    // Get Location -->latitude and longitude
    NSString *latitude = LATITUDE;
    NSString *longitude = LONGITUDE;
    CLLocationCoordinate2D coordinate = [self getLocation];
    if (coordinate.latitude != 0 && coordinate.longitude != 0)
    {
        latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
        longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    }

    // Create URL for get Today's Weather --> Host + lat + long + unitType + appid
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@lat=%@&lon=%@&units=metric&appid=%@",API_HOST,latitude,longitude,APP_ID]];
    
    // Create URL for get 7 days weather
     NSURL *urlWeek = [NSURL URLWithString:[NSString stringWithFormat:@"%@lat=%@&lon=%@&units=metric&cnt=7&appid=%@",API_WEEK,latitude,longitude,APP_ID]];

    [self makeapiRequestWithURL:url];
    [self makeapiRequestWithURL:urlWeek];
}

-(void)makeapiRequestWithURL:(NSURL *)url{
    // Create Request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    // Add Requst to data Task Featch Weather Data
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // handle request error
        if (error) {
            NSLog(@"Error: %@", error); // Error in response
            return;
        }
        
        NSError *jsonError;
        NSMutableDictionary *jsonResponse = [[NSMutableDictionary alloc]initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError]];
        
        if (jsonError) {
            NSLog(@"Json Conversion failed:");  //Json Error
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Json %@:",jsonResponse);
            [self updateUIWithResponse:jsonResponse]; // Pass response data to updated UI
        });
        
    }];
    [dataTask resume];
    NSLog(@"Task:%@", dataTask);
}

#pragma - mark Update UI

-(void)updateUIWithResponse:(NSMutableDictionary *)response{
    
    NSArray *list = [response objectForKey:@"list"];
    if(list.count != 0){
        weatherArray = [[NSMutableArray alloc]init]; // Creating array what addeed 7 days of data
        for (NSDictionary *dic in list) {
            NSString *date = [dic objectForKey:@"dt"];
            NSDictionary *temp =[dic objectForKey:@"temp"];
            NSString *tempMax = [temp objectForKey:@"max"];
            NSString *tempMin = [temp objectForKey:@"min"];
            NSArray  *weatherArra = [dic objectForKey:@"weather"];
            NSDictionary *weatherDic = [weatherArra objectAtIndex:0];
            NSString *imageName = [[self imageMap] objectForKey:[weatherDic objectForKey:@"icon"]];
            
            NSDictionary *weatherData = @{
                                          @"date":date,
                                          @"tempMax":tempMax,
                                          @"tempMin":tempMin,
                                          @"image":imageName
                                          };
            [weatherArray addObject:weatherData];
        }
        [_ibWeekWeather reloadData];  // Reload Cell when get the reponse from server
        return;
    }
    
    // Update UI
    NSString *temp = [NSString stringWithFormat:@"%ld",[[[response objectForKey:@"main"] objectForKey:@"temp"] integerValue]];
    NSString *tempMin = [NSString stringWithFormat:@"%ld",[[[response objectForKey:@"main"] objectForKey:@"temp_min"] integerValue]];
    NSString *tempMax = [NSString stringWithFormat:@"%ld",[[[response objectForKey:@"main"] objectForKey:@"temp_max"] integerValue]];
    NSString *sunrise = [NSString stringWithFormat:@"%@",[[response objectForKey:@"sys"] objectForKey:@"sunrise"]];
    NSString *sunset = [NSString stringWithFormat:@"%@",[[response objectForKey:@"sys"] objectForKey:@"sunset"]];
    NSArray  *weatherArra = [response objectForKey:@"weather"];
    NSDictionary *weatherDic = [weatherArra objectAtIndex:0];
    NSString *weather = [NSString stringWithFormat:@"%@",[weatherDic objectForKey:@"description"]];
    NSString *imageName = [[self imageMap] objectForKey:[weatherDic objectForKey:@"icon"]];
    
    lastUpdated = [NSDate date];

    
    _imageWeather.image = [UIImage imageNamed:imageName];
    _ibTemp.text = [NSString stringWithFormat:@"%@°",temp];
    _ibTempMax.text = [NSString stringWithFormat:@"%@°",tempMax];
    _ibTempMin.text = [NSString stringWithFormat:@"%@°",tempMin];
    _ibWeather.text = weather;
    _ibSunRiase.text = [self getTimeInterval: [NSDate dateWithTimeIntervalSince1970:[sunrise doubleValue]]];
    _ibSunSet.text =  [self getTimeInterval: [NSDate dateWithTimeIntervalSince1970:[sunset doubleValue]]];
    
  
}


#pragma - mark  Get Location
// Featch Current user location
-(CLLocationCoordinate2D)getLocation{
    if (locationManager == nil)
    {
        locationManager = [[CLLocationManager alloc] init];
    }
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
 
    if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
        [locationManager requestWhenInUseAuthorization];
    }else{
        [locationManager startUpdatingLocation];
    }
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
 
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error){
         CLPlacemark *placemark = [placemarks lastObject];
        NSLog(@"%@",placemark.locality);
        dispatch_async(dispatch_get_main_queue(), ^{
            if (placemark.locality) {
                _ibPlaceMark.text = placemark.locality;
            }
        });
     }];
    return coordinate;
}

#pragma - mark  UICollecation delegate

// Create UIcollcation cell to display One week data
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 7;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
 
    //Load Cell from nib
    WEWeekCollectionViewCell *cell = (WEWeekCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CustomCell"
                                  forIndexPath:indexPath];
    NSDictionary *weather = [weatherArray objectAtIndex:indexPath.row];
    if (weather != nil)
    {
        NSString *date = [weather objectForKey:@"date"];
        cell.ibDay.text = [self GetDayFromDate:[NSDate dateWithTimeIntervalSince1970:[date doubleValue]]];
        cell.ibTempMax.text = [NSString stringWithFormat:@"%ld°",[[weather objectForKey:@"tempMax"] integerValue]];
        cell.ibTempMin.text = [NSString stringWithFormat:@"%ld°",[[weather objectForKey:@"tempMin"] integerValue]];
        cell.ibimage.image = [UIImage imageNamed:[weather objectForKey:@"image"]];
    }
    cell.layer.cornerRadius = 5.0;
    cell.layer.masksToBounds = YES;
    
    cell.ibDay.hidden = NO;
    cell.ibTempMax.hidden = NO;
    cell.ibTempMin.hidden = NO;
    cell.ibimage.hidden = NO;
    cell.ibTodayLbl.hidden = YES;
    
    cell.backgroundColor = [UIColor colorWithRed:64/255.0f green:118/255.0f blue:150/255.0f alpha:1.0f];
    if (indexPath.row == 0) { // Hide UI if it is today Cell
         cell.ibDay.hidden = YES;
         cell.ibTempMax.hidden = YES;
         cell.ibTempMin.hidden = YES;
         cell.ibimage.hidden = YES;
         cell.ibTodayLbl.hidden = NO;
        cell.backgroundColor = [UIColor clearColor];
    }
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

#pragma mark - Get information
// Retuen Date from Day
-(NSString *)GetDayFromDate:(NSDate *)date{
    NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"EEE"];
    return [formatter stringFromDate:date];
}



// Get image for weather data
-(NSDictionary *)imageMap {
     NSDictionary *_imageMap = nil;
    if (! _imageMap) {
        _imageMap = @{
                      @"01d" : @"weather-clear",
                      @"02d" : @"weather-few",
                      @"03d" : @"weather-few",
                      @"04d" : @"weather-broken",
                      @"09d" : @"weather-shower",
                      @"10d" : @"weather-rain",
                      @"11d" : @"weather-tstorm",
                      @"13d" : @"weather-snow",
                      @"50d" : @"weather-mist",
                      @"01n" : @"weather-moon",
                      @"02n" : @"weather-few-night",
                      @"03n" : @"weather-few-night",
                      @"04n" : @"weather-broken",
                      @"09n" : @"weather-shower",
                      @"10n" : @"weather-rain-night",
                      @"11n" : @"weather-tstorm",
                      @"13n" : @"weather-snow",
                      @"50n" : @"weather-mist",
                      };
    }
    return _imageMap;
}

//Return Day in week 1-7
-(NSInteger)selecatedDate{
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    return [components weekday]-1;
}

// Updated every one sec UI last updated time
-(void)updateLastUpdatedTime{
    NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:lastUpdated];
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    NSString *lastupdated;
    if (hours != 0) {
        NSLog(@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds) ;
        lastupdated = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
    }else if (minutes != 0){
        lastupdated = [NSString stringWithFormat:@"%02ld:%02ld",(long)minutes, (long)seconds];
    }else{
        lastupdated = [NSString stringWithFormat:@"%02ld",(long)seconds];
    }
    NSString *updatedLast = [NSString stringWithFormat:@"Updated %@ seconds ago",lastupdated];
    _ibLastUpdatedTime.text = updatedLast;
    
}

// Get sinrise and sunset time
-(NSString *)getTimeInterval:(NSDate *)date{
    NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"HH:MM"];
    return [formatter stringFromDate:date];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

@end
