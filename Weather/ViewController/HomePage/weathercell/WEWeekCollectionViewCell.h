//
//  WEWeekCollectionViewCell.h
//  Weather
//
//  Created by Hari
//  Copyright © 2017 All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WEWeekCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *ibDay;
@property (strong, nonatomic) IBOutlet UILabel *ibTempMax;
@property (strong, nonatomic) IBOutlet UILabel *ibTempMin;
@property (strong, nonatomic) IBOutlet UILabel *ibTodayLbl;
@property (strong, nonatomic) IBOutlet UIImageView *ibimage;
@end
