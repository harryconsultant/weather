//
//  WEHomeViewController.h
//  Weather
//
//  Created by Hari
//  Copyright © 2017. All rights reserved.
//

#import <UIKit/UIKit.h>
#define API_HOST @"http://api.openweathermap.org/data/2.5/weather?"
#define API_WEEK @"http://api.openweathermap.org/data/2.5/forecast/daily?"
#define APP_ID @"4102726c0e1496579d699ada50683ec8"
#define API_KEy @"b1b15e88fa797225412429c1c50c122a1"
#define LATITUDE @"51.509980"
#define LONGITUDE @"-0.133700"

@interface WEHomeViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *ibLastUpdatedTime;
@property (strong, nonatomic) IBOutlet UILabel *ibPlaceMark;
@property (strong, nonatomic) IBOutlet UILabel *ibTemp;
@property (strong, nonatomic) IBOutlet UILabel *ibTempMax;
@property (strong, nonatomic) IBOutlet UILabel *ibTempMin;
@property (strong, nonatomic) IBOutlet UILabel *ibSunRiase;
@property (strong, nonatomic) IBOutlet UILabel *ibSunSet;
@property (strong, nonatomic) IBOutlet UILabel *ibWeather;
@property (strong, nonatomic) IBOutlet UICollectionView *ibWeekWeather;

@property (strong, nonatomic) IBOutlet UIImageView *imageWeather;

@end
