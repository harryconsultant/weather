//
//  main.m
//  Weather
//
//  Created by Hari
//  Copyright © 2017. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WEDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WEDelegate class]));
    }
}
